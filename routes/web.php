<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

//Route::name('blogs_path')->get('/blogs','BlogsController@index');

Route::name('create_blog_path')->get('/createblog','BlogsController@create');

Route::name('store_blog')->post('/store','BlogsController@store');

Route::name('blogs_index')->get('/','BlogsController@disp');

Route::name('edit_blog')->get('/show/{id}/edit','BlogsController@edit');

Route::name('update_blog')->put('/show/{id}','BlogsController@update');

Route::name('blog_path')->get('/show/{id}','BlogsController@show');



Route::name('category_index')->get('/dispcategory','CategoryController@disp');

Route::name('create_category_path')->get('/createcategory','CategoryController@create');

Route::name('store_category_path')->post('/storecategory','CategoryController@store');

Route::name('category_path')->get('/dispcategory/{category_id}','CategoryController@show');

Auth::routes();

//Route::get('/home', 'BlogsController@disp')->name('home');
