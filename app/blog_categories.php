<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog_Categories extends Model
{
    protected $table = "blog_categories";
    protected $primaryKey = "id";

    public $timestamps=false;

}
