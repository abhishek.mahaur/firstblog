<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Storage;

use App\Blog;
use App\Category;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function disp()
    {
        $data= Category::all();

        return view('category.categories',['data'=>$data]);
    }

    public function create()
    {
        return view('category.createcategory');
    }

    public function store(Request $request)
    {
        $category = new Category;

        $category->category= $request->input('category');

        $category->save();
        return redirect('/dispcategory');
            
    }

    public function show($category_id)
    {   
        $category = Category::find($category_id);
        if($category !== null)
        {
            $wholedata = $category->blogs;
            return view('blogs.index',['wholedata' => $wholedata]);
        }

        //echo heelo;
        //$blog=Blog::find($category_id);
       //return view('category.showcategory',['blog'=>$blog]);
    }
}
