<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Storage;

use Auth;
use App\Blog;
use App\Category;

class BlogsController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    
    public function disp()
    {
        $wholedata=DB::table('blog')->orderBy('bupdate','desc')->get();  
        //$wholedata= Blog::all();
        //foreach($wholedata as $data)
        /*{
            echo $data->title;
            echo"<br>";
            echo $data->author;
            echo"<br>";
            echo $data->content;
            echo"<br>";
        }*/
        return view('blogs.index',['wholedata'=>$wholedata]);
        /*$data=DB::select(DB::raw('select *FROM blog ORDER BY bdate desc'));
        echo"<pre>";
        var_dump($data);
        echo"</pre>";*/       
    }
    public function show($id)
    {
        $blog= Blog::find($id);
        return view('blogs.show',['blog'=>$blog]);
    }

    /*public function index()
    {
         return view('blogs.index');
    }*/

    public function create()
    {
        $user=Auth::user();
        $cat= Category::all();
        return view('blogs.create',['cat'=>$cat],['user'=>$user]);
    }

    public function store(Request $request)
    {
        /*$this->validate($request,[
            'author'=>'required',
            'title'=>'required' ,
            'content'=>'required'
        ]);*/
        //print_r($request->input());
        //return $request->all();    
        
        $blog = new Blog;
        $user=Auth::user();
        /*$author= $request->input('author');
        $title= $request->input('title');
        $content= $request->input('content');
  
        $data=array('author'=>$author,'title'=>$title,'content'=>$content);
  
        DB::table('blog')->insert($data);
         echo "sucess";*/
        $path= Storage::putFile('public',$request->file('images'));
        $url= Storage::url($path);
        
        $blog->image=$url;
       // $blog->category_id= $request->input('category_id');
        $blog->author= $user->name;
        $blog->title=  $request->input('title');
        $blog->content=  $request->input('content');  
           
        /*$blog->author= " author";
        $blog->title=  "title";
        $blog->content=  "content";    */
        

        $blog->save();
        $blog->category()->sync($request->category_id,false);
    

        return redirect('/');
            
        

    }
    public function edit($id)
    {
        //$cat= Category::all();
        $blog=Blog::find($id);
        return view('blogs.edit',['blog'=>$blog]);
    }
    
    public function update(Request $request,$id)
    {   
        $blog= Blog::find($id);

        //$blog->author=$request->input('author');
        $blog->title=$request->input('title');
        $blog->content=$request->input('content');
        $blog->update();
        
        return redirect('/');
        /*Blog::where('title',$request->title)
        ->update('content',input($request->content));*/
    }
}
