<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $table = "blog";
    protected $primaryKey = "id";
    //protected $fillable=['author','title','content','image'];

    const CREATED_AT='bdate';
    const UPDATED_AT='bupdate';

    public function category()
    {
        return $this->belongsToMany('App\Category','blog_categories'); 
    }
}
