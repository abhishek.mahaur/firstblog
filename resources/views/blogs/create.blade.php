@extends ('layouts.app')

@section('content')

<form action="{{url('/store')}}" method="POST" enctype="multipart/form-data">

    @csrf

        

        <div class="form-group">
          <label for="title">Title</label>
          <input type="text" name="title" class="form-control">
        </div>

        <div class="form group">
          <label for="category_id">Categories:</label>
          <select name="category_id[]" classs="form control" type="category_id" id="category_id" multiple>
            @foreach ($cat as $item)
          <option value="{{$item->id}}">{{$item->category}}</option>
          @endforeach
          </select>
        </div>
        
        <div class="form-group">
          <label for="content">Content</label>
          <textarea name="content" rows="10" class="form-control"></textarea>
        </div>

        <div class="form-group">
          <input type="file" name="images" class="form-control">
        </div>
        
        <button type="submit" class="btn btn-primary">Add blog post</button>
</form>




@endsection