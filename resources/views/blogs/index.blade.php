@extends ('layouts.app')

@section('content')

    @foreach ($wholedata as $blog)
    
    <!--
      <div class="card">
    
        <img src="{{ asset($blog->image) }}" class="card-img-" alt="">
        
        <div class="card-body">

            <h5 class="card-title">
            <a href="/show/{{$blog->id}}">
             {{$blog->title}} 
              </a>
            </h5>
          <h6 class="card-subtitle mb-2 text-muted">{{$blog->author}}</h6>
          <h6 class="card-subtitle mb-2 text-muted">Updated at-{{$blog->bupdate}}</h6>
          <p class="card-text">{{$blog->content}}</p>
    
        </div>
    </div>
      <br>
  -->
      <div class="card mb-3" style="">
        <div class="row no-gutters">
          <div class="col-md-4">
            <a href="/show/{{$blog->id}}">
            <img src="{{ asset($blog->image) }}" class="card-img-top" alt="">
            </a>
          </div>
          <div class="col-md-8">
            <div class="card-body">
              <h5 class="card-title">
                <a href="/show/{{$blog->id}}">
                 {{$blog->title}} 
                  </a>
                </h5>
              <h6 class="card-subtitle mb-2 text-muted">{{$blog->author}}</h6>
              <h6 class="card-subtitle mb-2 text-muted">Last updated at-{{$blog->bupdate}}</h6>
              </div>
          </div>
        </div>
      </div>
    <!--

    <div class="card">

      <div class="card header">
        <h1>{{$blog->title}}</h1>
      </div>
      <div class="card subtitle">
        <h4>{{$blog->author}}</h4>
      </div>
      <br>
      <div class="card body">
      <h6>{{$blog->content}}</h6>
      </div>
      <br><hr>
    -->
    @endforeach

@endsection
