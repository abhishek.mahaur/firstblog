@extends('layouts.app')

@section('content')

<!--
<div class="card">
    <div class="card-body">
        <h5 class="card-title">
         {{$blog->title}} 
        </h5>
      <h6 class="card-subtitle mb-2 text-muted">{{$blog->author}}</h6>
      <p class="card-text">{{$blog->content}}</p>
        
    </div>
    
</div>
-->

<div class="card mb-3" style="">
    <div>
      <div class="col-md-4">
        <img src="{{ asset($blog->image) }}" class="card-img" alt="">
      </div>
      <div class="col-md-8">
        <div class="card-body">
          <h5 class="card-title">
             {{$blog->title}} 
            </h5>
          <h6 class="card-subtitle mb-2 text-muted">{{$blog->author}}</h6>
          <h6 class="card-subtitle mb-2 text-muted">Last updated at-{{$blog->bupdate}}</h6>
          <p class="card-text">{{$blog->content}}</p>
          <h6>
            <p class="card-text">Categories : 
            @foreach ($blog->category as $cat)
          <a href="/dispcategory/{{$cat->id}}">  {{$cat->category}} </a>
          @endforeach
          </p>
        </h6>
        </div>
      </div>
    </div>
  </div>

<a href="/show/{{$blog->id}}/edit" class="btn btn-primary">Edit</a>
    <a href="{{route('blogs_index')}}" class="btn btn-primary">Back</a>
  
@endsection