@extends ('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
      <div class="col-md-8">
          <div class="card">
              <div class="card-header">Categories</div>

              <div class="card-body">
                  @if (session('status'))
                      <div class="alert alert-success" role="alert">
                          {{ session('status') }}
                      </div>
                  @endif
                  @foreach ($data as $item)
    
                  <div class="card">
                      
                      <div class="card-body">
                  
                          <h5 class="card-title">
                          <a href="/dispcategory/{{$item->id}}">
                           {{$item->category}} 
                          </a>
                          </h5>
                  
                      </div>
                  </div>
                    <br>
                  
                    @endforeach
              </div>
          </div>
      </div>
  </div>
</div>   


@endsection