@extends ('layouts.app')

@section('content')
    
<form action="{{route('store_category_path')}}" method="POST" >

    @csrf

        <div class="form-group">
        <label for="category" style="">Add Category</label>
        <input type="text" name="category" class="form-control">
        </div>

        <button type="submit" class="btn btn-primary">Add new category</button>
</form>


@endsection