@extends('layouts.app')

@section('content')

@foreach ($blogs as $item)
    
<div class="card mb-3" style="">
    <div class="row no-gutters">
      <div class="col-md-4">
        <img src="{{ asset($item->image) }}" class="card-img" alt="">
      </div>
      <div class="col-md-8">
        <div class="card-body">
          <h5 class="card-title">
             {{$item->title}} 
            </h5>
          <h6 class="card-subtitle mb-2 text-muted">{{$item->author}}</h6>
          <h6 class="card-subtitle mb-2 text-muted">Last updated at-{{$item->bupdate}}</h6>
          <p class="card-text">{{$item->content}}</p>
          <h6>
          <p class="card-text">Category: {{$item->category->category}}</p>
          </h6>
        </div>
      </div>
    </div>
  </div>

@endforeach
    <a href="{{route('category_index')}}" class="btn btn-primary">Back</a>
    
@endsection